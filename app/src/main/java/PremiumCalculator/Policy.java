
package PremiumCalculator;

import java.util.ArrayList;
/**
 * Policy class
 * Has policy number, status and array list with all of its child objects
 */
enum Policy_status
{
    REGISTRED,
    APPROVED,
    CANCELED
}

public class Policy {
    String policy_number;
    Policy_status policy_status;
    ArrayList<P_object> policy_objects ; 
    
    public Policy(String number,Policy_status status){
        this.policy_number = number;
        this.policy_status = status;
        this.policy_objects = new ArrayList<P_object>();
    }
    
   public  void addObject(P_object object){
       this.policy_objects.add(object);
       object.policy = this;
   }
   
   
}
