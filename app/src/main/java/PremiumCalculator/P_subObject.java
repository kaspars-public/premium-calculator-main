/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PremiumCalculator;


/**
 * Sub object class
 * Has name, insured sum or value, risk type and reference to parent object
 */
enum Risk_type {
    FIRE,
    THEFT
}

public class P_subObject {
    String name;
    double sum_insured;
    Risk_type risk_type;
    P_object parent;
    
    public P_subObject(String n,double sum, Risk_type type){
        this.name=n;
        this.sum_insured=sum;
        this.risk_type=type;
    }
}