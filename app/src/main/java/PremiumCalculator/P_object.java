
package PremiumCalculator;

import java.util.ArrayList;

/**
 * Policy object class
 * Has name, array list with child objects, reference to parent policy object
 */
public class P_object {
    String object_name;
    ArrayList<P_subObject>sub_objects;
    Policy policy;
    
    public P_object(String name){
        this.object_name = name;
        this.sub_objects = new ArrayList<P_subObject>();
    }
    
    public  void addSubObject(P_subObject subObject){
       this.sub_objects.add(subObject);
       subObject.parent = this;
   }
   
}
