/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package PremiumCalculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class P_objectTest {

    /**
     * Test of addSubObject method, of class P_object.
     */
    @Test
    public void testAddSubObject() {

        P_subObject subObject = new P_subObject("test sub object 1",254.42,Risk_type.FIRE);
        P_object instance = new P_object("test object 1");
        instance.addSubObject(subObject);
        
        assertSame(subObject,instance.sub_objects.get(0));
        assertSame(instance,instance.sub_objects.get(0).parent);
    }
    
}
