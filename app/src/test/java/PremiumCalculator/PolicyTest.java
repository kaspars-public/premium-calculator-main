
package PremiumCalculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PolicyTest {
    
    /**
     * Test of addObject method, of class Policy.
     */
    @Test
    public void testAddObject() {

        P_object object = new P_object("test object 1");
        Policy instance = new Policy("0001",Policy_status.REGISTRED);
        instance.addObject(object);
        
        assertSame(object,instance.policy_objects.get(0));
        assertSame(instance,instance.policy_objects.get(0).policy);
    }
    
}
