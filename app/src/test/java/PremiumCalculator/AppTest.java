
package PremiumCalculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class AppTest {
    
    static Policy policy1;
    static Policy policy2;
    
    public AppTest() {
    }
    
    // setup 2 policies with objects and subobjects for each coeificient case
    @BeforeAll
    public static void setUpClass() {
        // case 1
        policy1 = new Policy("001",Policy_status.APPROVED);    
        P_object object1 = new P_object("House");
        policy1.addObject(object1);
        
        P_subObject subObject1 = new P_subObject("TV",100.00,Risk_type.FIRE);
        object1.addSubObject(subObject1);
        P_subObject subObject2 = new P_subObject("Paiting",8.00,Risk_type.THEFT);
        object1.addSubObject(subObject2);    
        
        // case 2
        policy2 = new Policy("002",Policy_status.APPROVED);    
        P_object object2 = new P_object("House");
        policy2.addObject(object2);
        
        P_subObject subObject3 = new P_subObject("Computer",500.00,Risk_type.FIRE);
        object2.addSubObject(subObject3);
        P_subObject subObject4 = new P_subObject("Statue",102.51,Risk_type.THEFT);
        object2.addSubObject(subObject4);   
    }
    
    @AfterAll
    public static void tearDownClass() {
        // Java does automatic garbage collection, but just in case
        policy1 = null;
        policy2 = null;
    }
    
    /**
     * Test of getSums method, of class App.
     */
    @Test
    public void testGetSums() {

        double[] expResult1 = {100,8};
        double[] result1 = App.getSums(policy1);
        assertArrayEquals(expResult1, result1);

        double[] expResult2 = {500,102.51};
        double[] result2 = App.getSums(policy2);
        assertArrayEquals(expResult2, result2);
    }

    /**
     * Test of calculation method, of class App.
     */
    @Test
    public void testCalculation() {

        double expResult1 = 2.28;
        double result1 = App.calculation(policy1);
        assertEquals(expResult1, result1, 0.00);
        
        double expResult2 = 17.13;
        double result2 = App.calculation(policy2);
        assertEquals(expResult2, result2, 0.00);
    }
    
}
